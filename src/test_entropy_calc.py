import re
import math
import sys,time

import threading as T

def perm_bitstring(N):
    for i in xrange(0,2**N):
        #found using yield is better when there is potentially a lot of data to go through
        yield '{:0{n}b}'.format(i, n=N)

def normalize(v):
	vmag = math.sqrt(sum(v[i]*v[i] for i in range(len(v))))
	return [ v[i]/vmag  for i in range(len(v)) ]		
		
def calc(bitString,N=1):
    #Regular expression for 1's and 0's
    REGEX = re.compile('^(1|0)+$')
    entropies = []
    
    if REGEX.match(bitString):
        if N > len(bitString)/2 or N<1:
            N = len(bitString)/2
        print "Calculating up to N = "+str(N)
            
        #Loops over all subset lengths
        for sublen in range(1,N+1):
            count = {}
            total = 0
			#get permutation of pattern up until 
            for s in perm_bitstring(sublen):
                count[s] = 0
            #count up instances of each pattern at that length
            for i in range(0,len(bitString)-sublen+1):
                count[bitString[i:i+sublen]] += 1
                total += 1
			
            entropy = 0
            print count
            #convert totals to probabilities and calculate entropy
            for sub,value in count.items():
                prob = float(value)/total
                if prob > 0:
                    entropy -= prob*math.log(prob,2)

            #entropies.append(entropy/float(len(bitString)))
            entropies.append(entropy/float(sublen))
	
	print ""
	print "RESULT: "
	print str(entropies)
	return str(entropies)

if __name__=='__main__' :
    thr = T.Thread(target=calc,args=(sys.argv[1],),kwargs={'N':int(sys.argv[2])})
    thr.start()
    print "This should print before the result"


