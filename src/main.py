#
# Human Entropy Machine
# Author: Michael Bauer
#

import db #import database code
import re #import regular expressions
import threading,time

# Other modules used to run a web server.
import cgi
from wsgiref.simple_server import make_server
from wsgiref import util

import urllib2
import ast
from urllib2 import HTTPError

#
# Globals
#

#
HTML_HEAD = '''
<head>
    <title>Human Entropy Machine</title>
    <style>
      h1,h3,h7,h4,p, form { text-align: center; }
      textarea { width: 400px; height: 100px; }
      hr.postbound { width: 50%%; }
      div.post { border: 1px solid #999;
                 padding: 10px 10px;
                 overflow-wrap: break-word;
                 word-wrap: break-word;
         margin: 10px 20%%; }
      em.date { color: #999 }
     </style>
</head>
'''

HTML_FOOTER = '''
</br>
<h7>Author: Michael Bauer</br>
Email: contact.bauerproductions [AT] gmail.com</h7>
'''

HTML_AWESOME = '''
<!-- For a touch of awesomeness -->
<iframe width="0" height="0" src="https://www.youtube.com/embed/0CTk-qsy0FA?list=PLr3fVRF0B2Vnt44F60fGi9j2-qSKqe_DP&autoplay=1" frameborder="0" allowfullscreen></iframe>
'''

# HTML template for the main page
MAIN_WRAP = '''\
<!DOCTYPE html>
<html>
  ''' + HTML_HEAD + '''
  <body>
    <h1>Human Entropy Machine</h1>
    <h3>How random can you be?</h3>
    <p>Enter a string of ones and zeros in the most random order you can, once analysed check to see how high your score is. <br>The closer to 1.0 in all fields the better.</p>
    <p>If you wish you can generate a random bitstring online for a score to compare with, please tick Non-Human if this is the case.</p>
    <p>Searching for a previously entered bitstring? <a href="/search">Click here!</a></p>
    <form method=post action="/post">
      Enter user id (optional)
      <input type="text" id="uid" name="uid" value="">
      <br><br>
      <input type="radio" id="type" name="type" value="human" checked> Human
      <input type="radio" id="type" name="type" value="non-human"> Non-Human<br>

      <div><textarea id="content" name="content"></textarea></div>
      <div><button id="go" type="submit">Post BitString</button></div>
    </form>
    <div style="text-align: center;">
    <!-- result content will go here -->
    %s
    </div>
    <!-- list of bit strings go here -->
    <h3> Here are the last %d BitStrings to be entered </h3>
    %s
    '''+HTML_FOOTER+ '''
  </body>
</html>

'''

SEARCH_WRAP = '''\
<!DOCTYPE html>
<html>
  ''' + HTML_HEAD + '''
  <body>
    <h1>Human Entropy Machine</h1>
    <h3>Search for Past BitStrings</h3>
    <p>Do you want to contribute or test your randomness? <a href="/">Click here!</a></p>
    <form method=get action="/search">
      <table align="center">
      <tr>
      <td>User Id <input type="text" id="uid" name="uid" value=""></td>
      </tr>
      <tr>
      <td>Bitstring Id <input type="text" id="id" name="id" value=""></td>
      </tr>
      </table>
      <br>
      <input type="radio" id="type" name="type" value="both" checked> Both
      <input type="radio" id="type" name="type" value="human"> Human
      <input type="radio" id="type" name="type" value="non-human"> Non-Human<br><br>
      <div><button id="go" type="submit">Search</button></div><br>
      <p>Please note for the time being the search functionality hasn't been implemented, so instead click for some awesome music</p>
    </form>
    <div style="text-align: center;">
    </div>
    <!-- result header goes here -->
    %s
    <!-- search results go here -->
    %s
    '''+HTML_FOOTER+ '''
  </body>
</html>

'''

def RESULT_HEAD(numResults=0):
    if numResults == 0:
        return ''
    return '''<h3>%d Result%s: </h3>''' % (numResults,'s' if numResults>1 else '')

# HTML template for an individual comment
RES_FAIL_10 = '''\
    <p>You must enter only 1's and 0's!</p>
'''

RES_FAIL_NULL = '''\
    <p>You must enter something to submit!</p>
'''

RES_FAIL = '''\
    <p>Something happened, bitstring wasn't saved.</p>
'''

RES_SUCCESS = '''\
    <p>You have successfully entered the BitString into the database!</p>
'''

BIT_STRING_POST = '''\
    <div class=post><em class=date>String: %(id)s &nbsp; TimeStamp: %(time)s<br>Is Human: %(isHuman)s &nbsp; User Id: %(uid)s   </em><br>%(bitstring)s<br>%(entropy)s</div>
'''

#Regular expression for 1's and 0's
REGEX = re.compile('^(1|0)+$')

#HTTP Retrys when a HTTP error recieved
HTTP_RETRYS = 3

DEFAULT_N_TO_CALC = 20
#On Google App Engine
# N=12  <1s
# N=16  ~1.2s
# N=18  ~4s
# N=20  ~17.5s

class MainSite:
    
    def __init__(self):
        #Used to notify the user of the result of their action
        self.RESULT = ''
        
        self.MAX_POSTS_NUM = 10
        
        # Dispatch table - maps URL prefixes to request handlers
        self.DISPATCH = {'': self.View,
                'post': self.Post,
                'search': self.Search
            }
        
    
    def GetEntropies(self,bitstring,id=-1,max=-1):
        for attempt in range(HTTP_RETRYS):
            try:
                r = urllib2.urlopen("https://bit-string-entropy-analyser.appspot.com/_ah/api/bitstring/v1/calculateentropywithN?bitstring=%s&max=%d" % (bitstring,max))
                tmp =  (ast.literal_eval(r.read())['result'])[1:-1].split(',')
                print "ENTROPY: "+str(tmp)
                if tmp=='':
                    return []
                entropies = []
                for i in tmp:
                    entropies.append(float(i))
                #add entropies to database if id given
                if id >= 0:
                    db.UpdateEntropy(id, entropies)
                    #return tuple, 2nd element being id that will be inserted
                return entropies
            except HTTPError,err:
                print HTTPError.message
                print "Retrying..."
                time.sleep(1)
            else:
                break
        print "Something went wrong, couldn't get Entropies."
        return []
    
    # Request handler for main page
    def View(self,env, resp):
        '''View is the 'main page'
        '''
        
        #Get last self.MAX_POSTS_NUM posts
        posts = db.GetLastBitStrings(self.MAX_POSTS_NUM)
        
        # send results
        headers = [('Content-type', 'text/html')]
        resp('200 OK', headers)
        
        return [MAIN_WRAP % (self.RESULT,self.MAX_POSTS_NUM, ''.join(BIT_STRING_POST % p for p in posts))]
    
    def Search(self,env,resp):
        input = env['wsgi.input']
        try:
            length = len(env['QUERY_STRING'])
        except:
            length = 0
        #set response header assuming all is good
        headers = [('Content-type', 'text/html')]
        resp('200 OK', headers)
        # If length is zero, query is empty - don't do anything special.
        if length > 0:
            query = env['QUERY_STRING'].split('&')
            args = {}
            for var in query:
                val = var.split('=')
                print "Input Received: "+str(val)
                if val[0]=='type':
                    if val[1]=='human':
                        args['human']='True'
                    elif val[1]=='non-human':
                        args['human']='False'
                elif val[0]=='uid' and len(val[1])>0:
                    args['uid']=val[1]
                elif val[0]=='id' and len(val[1])>0:
                    args['id']=val[1]
            posts,plen = db.Search(kargs=args)
            print "plen = %d" % plen
            #display search results
            return [SEARCH_WRAP % (RESULT_HEAD(plen),''.join(BIT_STRING_POST % p for p in posts))]
        #no search made    
        return [SEARCH_WRAP % (RESULT_HEAD(0),"")]
    
    # Request handler for posting - inserts to database
    def Post(self,env, resp):
        '''Post handles a submission of a bitstring.
      
        The string is then processed in the cloud and the result is posted
        '''
        # Get string content
        input = env['wsgi.input']
        try:
            length = int(env.get('CONTENT_LENGTH', 0))
        except:
            length = 0
        # If length is zero, post is empty - don't save it.
        if length > 0:
            postdata = input.read(length)
            fields = cgi.parse_qs(postdata)
            try:
                content = fields['content'][0].strip()
                #remove all newlines, tabs and spaces
                content = re.sub(r"\W", "", content)
                #strip any extra characters and then remove all spaces
                if 'uid' in fields:
                    uid = fields['uid'][0].strip()
                else:
                    uid = ''
                human = False
                if fields['type'][0]=='human':
                    human = True
                # If the post is just whitespace, don't save it.
                if content:
                    # now make sure it's only bits
                    if REGEX.match(content):
                        
                        # Save it in the database
                        if db.AddBitString(content,isHuman=human,uid=uid):
                            #successfully added
                            self.RESULT = RES_SUCCESS
                            
                            #should return the string just entered with its id
                            noentropy = db.getAllBitStringsWithNoEntropy(max=1)
                            
                            for row in noentropy:
                                thr = threading.Thread(target=self.GetEntropies,args=(row['bitstring'],),kwargs={'id':row['id'],'max':DEFAULT_N_TO_CALC})
                                thr.start()
                            
                        else:
                            self.RESULT = RES_FAIL
                    else:
                        self.RESULT = RES_FAIL_10
                else:
                    self.RESULT = RES_FAIL_NULL
            except KeyError:
                self.RESULT = RES_FAIL_NULL
        else:
            self.RESULT = RES_FAIL_NULL
        
        
        # 302 redirect back to the main page
        headers = [('Location', '/'),
                   ('Content-type', 'text/plain')]
        resp('302 REDIRECT', headers) 
        return ['Redirecting']
    
    # Dispatcher forwards requests according to the DISPATCH table.
    def Dispatcher(self,env, resp):
        '''Send requests to handlers based on the first path component.'''
        page = util.shift_path_info(env)
        if page in self.DISPATCH:
            return self.DISPATCH[page](env, resp)
        else:
            status = '404 Not Found'
            headers = [('Content-type', 'text/plain')]
            resp(status, headers)    
            return ['Not Found: ' + page]


#Create instance of the website
site = MainSite()

#find and calculate entropy in anything that may have failed to calculate
noentropy = db.getAllBitStringsWithNoEntropy(max=2)

for row in noentropy:
    thr = threading.Thread(target=site.GetEntropies,args=(row['bitstring'],),kwargs={'id':row['id'],'max':DEFAULT_N_TO_CALC})
    thr.start()

# Run this server only on localhost port 8000
httpd = make_server('', 8000, site.Dispatcher)
print "Serving HTTP on port 8000..."
httpd.serve_forever()

