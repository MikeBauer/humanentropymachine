CREATE TABLE IF NOT EXISTS bitstrings ( 
    id          SERIAL      PRIMARY KEY,
    bitstring   TEXT        NOT NULL,
    entropy     real[],
    uid         TEXT,
    human       boolean     NOT NULL,
    time        TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);