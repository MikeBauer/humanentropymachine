#
# Database access functions
# 

import psycopg2
import bleach

## Database connection
def GetLastBitStrings(num):
    '''Get all the bitstrings from the database, sorted with the newest first.

    Returns:
      A list of dictionaries, where each dictionary corresponds to a row in the result.
    '''
    try:
        db = psycopg2.connect("dbname=bitdb")
        cur = db.cursor()
        cur.execute("SELECT bitstring,time,id,entropy,uid,human FROM bitstrings ORDER BY time DESC LIMIT %s",(num,))
        
        strings = ({'bitstring': str(bleach.clean(row[0])), 
                    'time': str(row[1]), 
                    'id': str(row[2]),
                    'uid': str(bleach.clean(row[4])),
                    'isHuman': str(row[5]),
                    'entropy': str(row[3])} 
                   for row in cur.fetchall())            
        db.close()
        return strings
    except:
        print "Unable to connect to database"
        
    return []

def Search(limit=250,kargs={}):
    '''
    A generic search function, accepts kargs of the same key as columns and with the desired value.
    Returns:
      A list of dictionaries, where each dictionary corresponds to a row in the result.
      The results correspond to the given parameters.
    '''
    try:
        db = psycopg2.connect("dbname=bitdb")
        cur = db.cursor()
        #start of statement
        query = "SELECT bitstring,time,id,entropy,uid,human FROM bitstrings "
        vals = []
        if len(kargs)>=1:
            query += "where "
        #add any parameters generically
        for key,value in kargs.items():
            query+= key+" = %s and "
            vals.append(value)
        #chop off last 'and'
        query = query[:-4]+" "
        #end of statement
        vals.append(limit)
        query += "ORDER BY time DESC LIMIT %s"
        print query % tuple(vals)
        cur.execute(query,tuple(vals))
        
        strings = ({'bitstring': str(bleach.clean(row[0])), 
                    'time': str(row[1]), 
                    'id': str(row[2]),
                    'uid': str(bleach.clean(row[4])),
                    'isHuman': str(row[5]),
                    'entropy': str(row[3])} 
                   for row in cur.fetchall())  
        plen = cur.rowcount          
        db.close()
        return strings,plen
    except:
        print "Unable to connect to database"
        raise
        
    return []

def getAllBitStringsWithNoEntropy(max=9999):
    '''Retrieves all bitstrings from the database with no entropy values
    
    Returns:
      A list of dictionaries, where each dictionary corresponds to a row in the database with no entropy values
      The id and bitstring is returned
    '''
    try:
        db = psycopg2.connect("dbname=bitdb")
        cur = db.cursor()
        cur.execute("SELECT id, bitstring FROM bitstrings WHERE entropy='{}' ORDER BY id DESC LIMIT %s", (max,))
        
        strings = ({'bitstring': str(bleach.clean(row[1])), 
                    'id': str(row[0])} 
                   for row in cur.fetchall()) 
        
        db.close()
        return strings
    except:
        print "Unable to connect to database"
    return []
    
def UpdateEntropy(id,entropy):
    '''Updates a bitstring with calculated entropy values
    
    '''
    
    try:
        db = psycopg2.connect("dbname=bitdb")
        cur = db.cursor()
        print "Updating bitstring id: "+id+" with Entropy:"+str(entropy)
        cur.execute("UPDATE bitstrings SET entropy=%s WHERE id=%s", (entropy,id))
        db.commit()
        db.close()
        return True
    except:
        print "Unable to connect to database"
        return False
    
def AddBitString(bitstring,entropy=[],isHuman=True,uid=""):
    '''Add a new bitstring to the database.
    
    '''
    #clean the dirty dirty data
    uid = bleach.clean(uid)
    bitstring = bleach.clean(bitstring)
    try:
        db = psycopg2.connect("dbname=bitdb")
        cur = db.cursor()
        print "Inserting bitstring: "+bitstring+" into database."
        cur.execute("INSERT INTO bitstrings (bitstring,entropy,human,uid) VALUES (%s,%s,%s,%s)", (bitstring,entropy,isHuman,uid))
        db.commit()
        db.close()
        return True
    except:
        print "Unable to connect to database"
        return False
