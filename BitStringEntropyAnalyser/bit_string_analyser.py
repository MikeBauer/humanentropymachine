import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

import math
import re

# If the request contains path or querystring arguments,
# you cannot use a simple Message class.
# Instead, you must use a ResourceContainer class
REQUEST_CONTAINER = endpoints.ResourceContainer(
    message_types.VoidMessage,
    bitstring=messages.StringField(1),
)

REQUEST_CONTAINER_N = endpoints.ResourceContainer(
    message_types.VoidMessage,
    bitstring=messages.StringField(1),
    max=messages.IntegerField(2),
)

package = 'BitString'


class EntropyList(messages.Message):
    """String that stores a message."""
    result = messages.StringField(1)

@endpoints.api(name='bitstring', version='v1')
class EntropyCalculator(remote.Service):
    """Bit String Entropy Analyser API v1."""

    @endpoints.method(REQUEST_CONTAINER, EntropyList,
                      path = "calculateentropy", http_method='GET', name = "calculateEntropy")
    def calculate_entropy(self, request):
        input = request.bitstring
        return EntropyList(result=self.calc(input))
    
    @endpoints.method(REQUEST_CONTAINER_N, EntropyList,
                      path = "calculateentropywithN", http_method='GET', name = "calculateEntropyWithN")
    def calculate_entropy_N(self, request):
        input = request.bitstring
        N = request.max
        return EntropyList(result=self.calc(input,N))
    
    def perm_bitstring(self,N):
        for i in xrange(0,2**N):
            #found using yield is better when there is potentially a lot of data to go through
            yield '{:0{n}b}'.format(i, n=N)
            
    #I think I may need this function 
    def normalize(self,v):
        vmag = math.sqrt(sum(v[i]*v[i] for i in range(len(v))))
        return [ v[i]/vmag  for i in range(len(v)) ]


    def calc(self,bitString,N=-1):
        """Calculates the entropy of a bitstring across the entire bitstring as a substring or until N. 
        Complexity is O(N!)
        Returns a string representing the array of entropies of length N"""
        
        #Regular expression for 1's and 0's
        REGEX = re.compile('^(1|0)+$')
        entropies = []
        
        if REGEX.match(bitString):
            #We don't want to calculate substrings any more than half the length of the input string
            #for reasons...
            if N > len(bitString)/2 or N<1:
                N = len(bitString)/2
            
            #Loops over all subset lengths
            for sublen in range(1,N+1):
                count = {}
                total = 0
                #get permutation of pattern up until 
                for s in self.perm_bitstring(sublen):
                    count[s] = 0
                
                #count up instances of each pattern at that length
                for i in range(0,len(bitString)-sublen+1):
                    count[bitString[i:i+sublen]] += 1
                    total += 1
                
                entropy = 0
                
                #convert totals to probabilities and calculate entropy
                for sub,value in count.items():
                    prob = float(value)/total
                    if prob > 0:
                        entropy -= prob*math.log(prob,2)
                    
                entropies.append(entropy/float(sublen))
                
        
        return str(entropies)


APPLICATION = endpoints.api_server([EntropyCalculator])

